# Support

> Support for [solid.community](https://solid.community/) pod server

This repository is a work in progress.  

Currently, the aims are:

- **[Knowledge base](https://gitlab.com/solid.community/support/-/wikis/home):** Information about the pod server
- **[New issues](https://gitlab.com/solid.community/support/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=):** add a support issue
- **[History](https://gitlab.com/solid.community/support/issues):** Track what was done and when
- **[Admin Guide](https://gitlab.com/solid.community/support/-/wikis/home#admin-guide):** guide for admins to help solve community issues

[**File an issue →**](https://gitlab.com/solid.community/support/issues)

Issues raised here should be of a short lived support nature.  Longer lived
issues should be filed in the [proposals](https://gitlab.com/solid.community/proposals) repo